import EventEmitter from 'eventemitter3';
import config from '../../config';
import delay from '../utils';
import Person from './Person';

export default class Planet extends EventEmitter {
  constructor(name, peopleData) {
    super();
    this.name = name;
    this.config = config;
    this.peopleData = peopleData;
    this.population = [];
  }

  static get events() {
    return {
      PERSON_BORN: 'person_born',
      POPULATING_COMPLETED: 'population_completed',
    };
  }

  get populationCount() {
    return this.population.length;
  }

  populate() {
    this.peopleData.forEach(async (person) => {
      await delay(this.config.populationDelay);
      this.population.push(new Person(person.name, person.height, person.mass));
      this.emit(Planet.events.PERSON_BORN, { filmUrls: person.films });
      if (this.populationCount === this.peopleData.length) {
        this.emit(Planet.events.POPULATING_COMPLETED);
      }
    });
  }
}
