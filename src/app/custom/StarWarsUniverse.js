import EventEmitter from 'eventemitter3';
import Planet from './Planet';
import Film from './Film';

export default class StarWarsUniverse extends EventEmitter {
  constructor() {
    super();
    this.films = [];
    this.planet = null;
  }

  async init() {
    const url = 'https://swapi.booost.bg/api/planets/';
    let data = await fetch(url).then((response) => response.json());
    const planets = data.results;

    while (data.next) {
      data = await fetch(data.next).then((response) => response.json());
      data.results.forEach((element) => {
        planets.push(element);
      });
    }
    const zeroPlanet = planets.filter((el) => {
      if (el.population === '0') {
        return el;
      }
    });

    const people = await fetch('https://swapi.booost.bg/api/people/')
      .then((response) => response.json()).then((data) => data.results);

    this.planet = new Planet(zeroPlanet[0].name, people);
    this.planet.on(Planet.events.PERSON_BORN, (dataFilms) => { this._onPersonBorn(dataFilms)});
    this.planet.on(Planet.events.POPULATING_COMPLETED, () => {
      this.emit(StarWarsUniverse.events.UNIVERSE_POPULATED);
    });
    this.planet.populate();
  }

  _onPersonBorn(dataFilms) {
    dataFilms.filmUrls.forEach((url) => {
      const filmExists = this.films.filter((film) => {
        if (film.url === url) {
          return film;
        }
      });

      if (!filmExists.length) {
        this.films.push(new Film(url));
        this.emit(StarWarsUniverse.events.FILM_ADDED);
      }
    });
  }

  static get events() {
    return {
      FILM_ADDED: 'film_added',
      UNIVERSE_POPULATED: 'universe_populated',
    };
  }
}
