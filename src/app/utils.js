/**
 * Here you can define helper functions to use across your app.
 */

const delay = (number) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, 1000 * number);
  });
};

export default delay;
